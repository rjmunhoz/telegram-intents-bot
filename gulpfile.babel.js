/* eslint-disable no-console */
import gulp from 'gulp';
import babel from 'gulp-babel';
import sourcemaps from 'gulp-sourcemaps';
import eslint from 'gulp-eslint';
import { spawn } from 'child_process';
import chalk from 'chalk';

let child;

gulp.task('eslint', () => {
  gulp.src('src/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('babel', ['eslint'], () => {
  gulp.src('src/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist'));
});

gulp.task('default', ['babel'], () => {
  if (child) child.kill();
  child = spawn('node', ['dist/index.js'], { stdio: 'inherit' });
  child.on('exit', code => {
    console.log((!code || code == 0) ? chalk.yellow('Reiniciando') : chalk.red(`Processo saiu com código ${code}`));
  });
});

gulp.task('watch', ['default'], () => {
  gulp.watch('src/**/*.js', ['default']);
});