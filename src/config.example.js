// Copie este arquivo para config.js e preencha as variáveis abaixo com as informações do seu bot
const TELEGRAM_TOKEN = 'Token do seu bot no Telegram';
const LUIS_APPID = 'AppID do seu app no luis.ai';
const LUIS_SUBSKEY = 'SubcriptionKey do seu app no luis.ai';

export {
  TELEGRAM_TOKEN
  , LUIS_APPID
  , LUIS_SUBSKEY
};