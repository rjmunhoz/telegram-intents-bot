import { error, fatal } from './log';

export default function () {
  try {
    const config = require('../config');
    const exampleConfig = require('../config.example');

    for (let key in exampleConfig) {
      if (!(key in config) || config[key] == '') fatal(`Você não preencheu a variável ${key} nas configurações!`);
    }

  } catch (ex) {
    error('Erro ao carregar arquivo de configuração!');
    fatal(ex);
  }
}