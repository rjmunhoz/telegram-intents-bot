/* eslint-disable no-console */
import { red, cyan, blue, green } from 'chalk';

const log = text => console.log(blue(text));
const info = text => console.log(cyan(text));
const success = text => console.log(green(text));
const error = text => console.log(red(text));
const fatal = text => {
  console.log(red(text));
  process.exit(1);
};

export {
  log
  , info
  , success
  , error
  , fatal
};