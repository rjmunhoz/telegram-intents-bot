const replies = [
  'Opa! Como posso te ajudar?'
  , 'Eae! Em que posso ajudar?'
  , 'Eae jovem! Em que posso ser útil?'
];

const getRandomReply = () => replies[Math.floor(Math.random() * replies.length)];

const run = () => new Promise(res => res(getRandomReply()));

export {
  run
};