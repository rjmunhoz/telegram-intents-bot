import {log} from '../log';

const hasEntities = entities => ('led' in entities) && ('action' in entities);

const actions = {
  TURN_ON: 0
  , TURN_OFF: 1
  , BLINK: 2
  , PULSE: 3
}

const getAction = rawAction => {
  if (rawAction.match(/(?:acend(?:a|er)|(?:lig(?:ar|ue)))/)) return actions.TURN_ON;
  if (rawAction.match(/(?:apag(?:ar|ue)|(?:deslig(?:ar|ue)))/)) return actions.TURN_OFF;
  if (rawAction.match(/pis(?:car|que)/)) return actions.BLINK;
  if (rawAction.match(/puls(?:ar|e)/)) return actions.PULSE;
};

const run = entities => new Promise((res, rej) => {
  if (hasEntities(entities)) {
    let msg = '';
    const led = entities.led;

    switch (getAction(entities.action)) {
      case actions.TURN_ON:
        msg = `OK, ligando LED ${led}`;
        break;
      case actions.TURN_OFF:
        msg = `OK, desligando LED ${led}`;
        break;
      case actions.BLINK:
        msg = `OK, piscando LED ${led}`;
        break;
      case actions.PULSE:
        msg = `OK, pulsando LED ${led}`;
        break;
    }

    res(msg);
  } else {
    log(entities);
    rej('Hm... Acho que não entendi o que você quis dizer. O que é pra fazer, com qual led?');
  }
});

export {
  run
};