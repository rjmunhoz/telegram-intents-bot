import fs from 'fs';

const _intents = fs.readdirSync(__dirname).filter(f => f !== 'index.js');
const intents = new Map();

_intents.forEach(i => {
  const intent = i.split('.')[0];
  intents.set(intent, require(`./${intent}`));
});

export default intents;