import intents from './intents/';
import r from 'request-promise-native';

export default class Api {

  constructor(appId, subscriptionKey) {
    this.appId = appId;
    this.subscriptionKey = subscriptionKey;
    this.url = `https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/${appId}?subscription-key=${subscriptionKey}`;
  }

  doRequest(msg) {
    return new Promise((res, rej) => {
      r.get(`${this.url}&q=${msg.text}`, { json: true })
        .then(result => {
          if ('topScoringIntent' in result) {
            const intent = result.topScoringIntent.intent.toLowerCase();
            if (intents.has(intent)) {
              const _intent = intents.get(intent);
              _intent.run(msg)
                .then(res)
                .catch(rej);
            } else {
              res(`Não sei responder a isso :/\nPeça pro @rmunhoz criar o intent \`${intent}\``);
            }
          } else {
            res('Não entendi');
          }
        })
        .catch(rej);
    });
  }

}