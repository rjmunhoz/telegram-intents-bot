/* eslint-disable no-console */
import TelegramBot from 'node-telegram-bot-api';
import * as config from './config';
import Api from './lib/api';
import { error, info } from './lib/log';
import validateConfig from './lib/configvalidator';

validateConfig();

const bot = new TelegramBot(config.TELEGRAM_TOKEN, { polling: true });
const api = new Api(config.LUIS_APPID, config.LUIS_SUBSKEY);

bot.getMe()
  .then(me => {
    const _info = [];
    _info.push(`Bot ${me.username} em execução!`);
    _info.push(`ID: ${me.id}`);
    _info.push(`Nome: ${me.first_name}`);
    info(_info.join('\n'));
  })
  .catch(error);

bot.onText(/.*/, msg => {
  api.doRequest(msg)
    .then(reply => bot.sendMessage(msg.chat.id, reply, { parse_mode: 'Markdown' }))
    .catch(error);
});